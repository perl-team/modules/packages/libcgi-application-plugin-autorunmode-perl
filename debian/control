Source: libcgi-application-plugin-autorunmode-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcgi-pm-perl | perl (<< 5.19),
                     perl,
                     libcgi-application-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-autorunmode-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-autorunmode-perl.git
Homepage: https://metacpan.org/release/CGI-Application-Plugin-AutoRunmode

Package: libcgi-application-plugin-autorunmode-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcgi-application-perl
Breaks: libcgi-application-plugins-perl (<< 0.11)
Replaces: libcgi-application-plugins-perl (<< 0.11)
Description: CGI::App plugin to automatically register runmodes
 The CGI::Application::Plugin::AutoRunmode plugin for CGI::Application
 provides easy ways to setup run modes. You can just write the method that
 implements a run mode. You do not have to explicitly register it with
 CGI::Application anymore.
 .
 You can either flag methods in your CGI::App subclass with the "Runmode" or
 "StartRunmode" attributes or simply declare that every method in a class is a
 run mode. You can also assign a delegate object, all whose methods will
 become runmodes. You can also mix both approaches.
